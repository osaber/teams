package com.mubaloo.teams.presenter;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.mubaloo.teams.TeamsApplication;
import com.mubaloo.teams.rest.MubalooWebService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by saber on 2016. 07. 03..
 */
@RunWith(MockitoJUnitRunner.class)
public class SplashPresenterUnitTest {

    @Mock
    ConnectivityManager mockConnectivityManager;

    @Mock
    NetworkInfo mockNetworkInfo;

    @Mock
    SplashPresenter mockSplashPresenter;

    @Mock
    MubalooWebService mockMubalooWebService;


    @Test
    public void checkNetworkTest(){

        when(mockConnectivityManager.getActiveNetworkInfo()).thenReturn(null);
        TeamsApplication.setOfflineMode(false);
        assertThat(TeamsApplication.isOfflineMode(), is(false));

        when(mockConnectivityManager.getActiveNetworkInfo()).thenReturn(mockNetworkInfo);
        NetworkInfo activeNetworkInfo = mockConnectivityManager.getActiveNetworkInfo();
        when(activeNetworkInfo.isConnected()).thenReturn(false);
        TeamsApplication.setOfflineMode(true);
        assertThat(TeamsApplication.isOfflineMode(), is(true));

        when(activeNetworkInfo.isConnected()).thenReturn(true);
        TeamsApplication.setOfflineMode(false);
        assertThat(TeamsApplication.isOfflineMode(), is(false));
    }

    //TODO other test case

}
