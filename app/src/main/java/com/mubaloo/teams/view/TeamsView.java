package com.mubaloo.teams.view;

import android.content.Context;

/**
 * Created by saber on 2016. 06. 25..
 */
public interface TeamsView {

    Context getContext();
}
