package com.mubaloo.teams.view;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mubaloo.teams.TeamsApplication;
import com.mubaloo.teams.presenter.TeamMemberDetailPresenter;
import com.mubaloo.teams.R;
import com.mubaloo.teams.model.TeamMember;
import com.mubaloo.teams.rest.ProfileImageFromUrl;

/**
 * A fragment representing a single TeamMember detail screen.
 * This fragment is either contained in a {@link TeamsActivity}
 * in two-pane mode (on tablets) or a {@link TeamMemberDetailActivity}
 * on handsets.
 */
public class TeamMemberDetailFragment extends Fragment {

    private TeamMember selectedTeamMember;

    /**
     * The fragment argument representing the team member ID that this fragment
     * represents.
     */
    public static final String ARG_TEAM_MEMBER_ID = "selected_team_member_id";

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TeamMemberDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey("presenter")){
            TeamMemberDetailPresenter presenter = getArguments().getParcelable("presenter");
            presenter.prepareTeamMemberFromSQLiteDb();
            selectedTeamMember = presenter.getSelectedTeamMember();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.team_member_detail, container, false);

        if (selectedTeamMember != null){
            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(selectedTeamMember.getFirstName() + " " + selectedTeamMember.getLastName());
            }
            ImageView teamMumberImage = (ImageView) rootView.findViewById(R.id.team_member_image);
            if(!TeamsApplication.isOfflineMode()) {
                ProfileImageFromUrl profileImageFromUrl = new ProfileImageFromUrl(teamMumberImage);
                profileImageFromUrl.execute(selectedTeamMember.getProfileImageUrl());
            }
            ((TextView) rootView.findViewById(R.id.team_member_first_name)).setText(getString(R.string.first_name_label) + " " + selectedTeamMember.getFirstName());
            ((TextView) rootView.findViewById(R.id.team_member_last_name)).setText(getString(R.string.last_name_label) + " " + selectedTeamMember.getLastName());
            ((TextView) rootView.findViewById(R.id.team_member_role)).setText(getString(R.string.role_label) + " " + selectedTeamMember.getRole());
        }

        return rootView;
    }
}
