package com.mubaloo.teams.view;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.mubaloo.teams.TeamsApplication;
import com.mubaloo.teams.presenter.SplashPresenter;
import com.mubaloo.teams.presenter.SplashPresenterImpl;
import com.mubaloo.teams.R;

/**
 * Created by saber on 2016. 06. 26..
 */
public class SplashActivity extends AppCompatActivity implements SplashView{

    private Animation cotinueButtonAnim;
    private SplashPresenter presenter;
    private ImageView mubalooLogo;
    private static final int SPLASH_DURATION = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setResponsiveSplashLogo();
        presenter = new SplashPresenterImpl(this);
        View view = findViewById(R.id.continue_button);
        Button continueButton = (Button) view;
        presenter.checkNetwork();
        if(TeamsApplication.isOfflineMode()) {
            if(continueButton.getVisibility() != View.VISIBLE)
                continueButton.setVisibility(View.VISIBLE);
            cotinueButtonAnim = AnimationUtils.loadAnimation(this, R.anim.button_animation);
            continueButton.setAnimation(cotinueButtonAnim);
            continueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.startAnimation(cotinueButtonAnim);
                    presenter.navigateToTeamsView(SplashActivity.this);
                    finish();
                }
            });
        }else{
            if(continueButton.getVisibility() == View.VISIBLE)
                continueButton.setVisibility(View.INVISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    presenter.callMubalooWebService(presenter);
                }
            }, SPLASH_DURATION);

        }
    }

    private void setResponsiveSplashLogo() {
        mubalooLogo = (ImageView)findViewById(R.id.mubaloo_logo);
        mubalooLogo.getLayoutParams().width = (int) (getWindowSize(this).x * 0.75);
    }

    /**
     * @param context the application context
     * @return Point containing the width and height
     * @brief methods for getting device window height and width via Point object
     */
    public static Point getWindowSize(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    @Override
    public Context getContext() {
        return this;
    }
}
