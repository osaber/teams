package com.mubaloo.teams.view;

import android.content.Context;

/**
 * Created by saber on 2016. 06. 26..
 */
public interface SplashView {
    Context getContext();
}
