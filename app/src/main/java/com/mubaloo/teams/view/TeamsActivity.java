package com.mubaloo.teams.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ExpandableListView;


import com.mubaloo.teams.presenter.TeamsPresenter;
import com.mubaloo.teams.presenter.TeamsPresenterImpl;
import com.mubaloo.teams.R;

/**
 * An activity representing a list of team member. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link com.mubaloo.teams.view.TeamMemberDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */

/**
 * Created by saber on 2016. 06. 25..
 */
public class TeamsActivity extends AppCompatActivity implements TeamsView {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private TeamsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_member_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        ExpandableListView expandableListView = (ExpandableListView)findViewById(R.id.team_member_list);
        assert expandableListView != null;
        presenter = new TeamsPresenterImpl(this, expandableListView);
        presenter.prepareTeamListFromSQLiteDb();
        presenter.showTeamListOnScreen();
        if (findViewById(R.id.team_member_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
    }

    @Override
    public Context getContext() {
        return this;
    }
}
