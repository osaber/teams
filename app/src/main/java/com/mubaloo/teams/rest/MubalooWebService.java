package com.mubaloo.teams.rest;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.mubaloo.teams.presenter.SplashPresenter;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;



/**
 * Created by saber on 2016. 06. 25..
 */
public class MubalooWebService extends AsyncTask<String, Void, String> {

    private SplashPresenter presenter;
    private Context context;

    public MubalooWebService(Context context, SplashPresenter presenter){
        super();
        this.context = context;
        this.presenter = presenter;
    }

    public static String getResponse(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    @Override
    protected String doInBackground(String... urls) {

        try {
            return getResponse(urls[0]);
        } catch (IOException e) {
            return e.getLocalizedMessage();
        }
    }


    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        try {
            presenter.setMubalooWebServiceResponse(new JSONArray(result));
            presenter.saveResponseToSQLiteDb();
            presenter.navigateToTeamsView(context);
            ((Activity)context).finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




}
