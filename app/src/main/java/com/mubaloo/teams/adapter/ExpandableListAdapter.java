package com.mubaloo.teams.adapter;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mubaloo.teams.TeamsApplication;
import com.mubaloo.teams.R;
import com.mubaloo.teams.model.TeamMember;
import com.mubaloo.teams.rest.ProfileImageFromUrl;

/**
 * A Class for viewing Team and Team member lists on Expandable List
 * Created by saber on 2016. 06. 26..
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<TeamMember>> _listDataChild;

    /**
     * Constructor
     * @param context: a Context object to initialize _context variable
     * @param listDataHeader: a list of String objects to initialize _listDataHeader variable
     * @param listChildData: a map of String-TeamMember objects to initialize _listDataChild  variable
     */
    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<TeamMember>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        TeamMember teamMember = (TeamMember) getChild(groupPosition, childPosition);
        final String teamMumberName = teamMember.getFirstName()+ " "+teamMember.getLastName();
        final String teamMemberRole = teamMember.getRole();
        LayoutInflater infalInflater = (LayoutInflater) this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(teamMember.isTeamLead()){
            convertView = infalInflater.inflate(R.layout.team_lead, null);
        }else{
            convertView = infalInflater.inflate(R.layout.team_member, null);
        }

        TextView teamMumberNameView = (TextView) convertView
                .findViewById(R.id.team_member_name);
        teamMumberNameView.setText(teamMumberName);

        TextView teamMumberRoleView = (TextView) convertView
                .findViewById(R.id.team_member_role);
        teamMumberRoleView.setText(teamMemberRole);

        ImageView teamMumberImage = (ImageView) convertView.findViewById(R.id.team_member_image);
        if(!TeamsApplication.isOfflineMode()) {
            ProfileImageFromUrl profileImageFromUrl = new ProfileImageFromUrl(teamMumberImage);
            profileImageFromUrl.execute(teamMember.getProfileImageUrl());
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }
        TextView teamName = (TextView) convertView
                .findViewById(R.id.team_name);
        teamName.setTypeface(null, Typeface.BOLD);
        teamName.setText(headerTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}
