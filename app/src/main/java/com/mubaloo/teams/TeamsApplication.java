package com.mubaloo.teams;

import android.content.Context;

import com.activeandroid.ActiveAndroid;

/**
 * Created by saber on 2016. 06. 25..
 */
public class TeamsApplication extends com.activeandroid.app.Application {

    private static Context appContext;
    private static String webServiceUrl;
    private static boolean offlineMode;
    private static long selectedTeamMemberId;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        webServiceUrl = getString(R.string.web_service_url);
        ActiveAndroid.initialize(this);
    }

    public static Context getContext() {
        return appContext;
    }

    public static String getWebServiceUrl(){
        return webServiceUrl;
    }

    public static boolean isOfflineMode() {
        return offlineMode;
    }

    public static void setOfflineMode(boolean offlineMode) {
        TeamsApplication.offlineMode = offlineMode;
    }

    public static long getSelectedTeamMemberId() {
        return selectedTeamMemberId;
    }

    public static void setSelectedTeamMemberId(long selectedTeamMemberId) {
        TeamsApplication.selectedTeamMemberId = selectedTeamMemberId;
    }
}
