package com.mubaloo.teams.model;

import android.provider.BaseColumns;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Represents the model of "TeamMember" objects and "team_member" datatable
 * Created by saber on 2016. 06. 24..
 */
@Table(name = "team_member", id = BaseColumns._ID)
public class TeamMember extends Model{

    /**
     * Represents the team member remote ID (server side team member ID)
     */
    @Column(name = "remote_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String remoteId;

    /**
     * Represents the first name of the team member
     */
    @Column(name = "first_name")
    private String firstName;

    /**
     * Represents the last name of the team member
     */
    @Column(name = "last_name")
    private String lastName;

    /**
     * Represents the role in the team of the team member
     */
    @Column(name = "role")
    private String role;

    /**
     * Represents the url of team member profile image
     */
    @Column(name = "profile_image_url")
    private String profileImageUrl;

    /**
     * Represents which team the team member belongs
     */
    @Column(name = "team", onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    private Team team;

    /**
     * Represents the team member is team lead or not
     */
    @Column(name = "team_lead")
    private boolean teamLead;

    /**
     * Query the server side ID of the team member
     * @return
     */
    public String getRemoteId() {
        return remoteId;
    }

    /**
     *  Set the server side ID of the team member
     * @param remoteId
     */
    public void setRemoteId(String remoteId) {
        this.remoteId = remoteId;
    }

    /**
     * Query the first name of the team member
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the first name of the team member
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Query the last name of the team member
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the last name of the team member
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Query the role of the team member
     * @return
     */
    public String getRole() {
        return role;
    }

    /**
     * Set the of the team member
     * @param role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * Query the url of team member profile image
     * @return
     */
    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    /**
     * Set the url of team member profile image
     * @param profileImageUrl
     */
    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    /**
     * Query which team the team member belongs
     * @return
     */
    public Team getTeam() {
        return team;
    }

    /**
     * Set which team the team member belongs
     * @param team
     */
    public void setTeam(Team team) {
        this.team = team;
    }

    /**
     * Query the team member is team lead or not
     * @return
     */
    public boolean isTeamLead() {
        return teamLead;
    }

    /**
     * set the team member is team lead or not
     * @param teamLead
     */
    public void setTeamLead(boolean teamLead) {
        this.teamLead = teamLead;
    }

    /**
     * Allows to update the states of team member objects
     * @param teamMember
     */
    public void  update(TeamMember teamMember){
        this.remoteId = teamMember.getRemoteId();
        this.firstName = teamMember.getFirstName();
        this.lastName = teamMember.getLastName();
        this.role = teamMember.getRole();
        this.profileImageUrl = teamMember.getProfileImageUrl();
        this.team = teamMember.getTeam();
        this.teamLead = teamMember.isTeamLead();
        this.save();
    }
}
