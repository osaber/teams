package com.mubaloo.teams.model;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Represents the model of "Team" objects and "team" datatable
 * Created by saber on 2016. 06. 24..
 */
@Table(name = "team", id = BaseColumns._ID)
public class Team extends Model{

    /**
     * Represents the team name
     */
    @Column(name = "name")
    private String name;

    /**
     * Default constructor without parameters
     */
    public Team(){
        super();
    }

    /**
     * Query the team name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the team name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Query all team objects from SQLite database, team table
     * @return List<Team>
     */
    public static List<Team> all(){
        return  new Select().from(Team.class).execute();
    }

    /**
     * Query all team member of the team from SQLite database, team_member table
     * @return
     */
    public List<TeamMember> members(){
        return getMany(TeamMember.class, "Team");
    }
}
