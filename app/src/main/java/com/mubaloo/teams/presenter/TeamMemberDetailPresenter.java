package com.mubaloo.teams.presenter;

import android.os.Parcelable;

import com.mubaloo.teams.model.TeamMember;

/**
 * Presenter/ controller interface which controle the "TeamMemberDetailActivity" bihaviours
 * Created by saber on 2016. 06. 25..
 */
public interface TeamMemberDetailPresenter extends Parcelable {

    /**
     * Method to load selected team member from the device database and gives it's reference to "selectedTeamMember"
     */
    void prepareTeamMemberFromSQLiteDb();

    /**
     * Method to query the selected team member
     * @return selectedTeamMember: TeamMember object
     */
    TeamMember getSelectedTeamMember();
}
