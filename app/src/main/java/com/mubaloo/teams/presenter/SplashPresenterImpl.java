package com.mubaloo.teams.presenter;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.mubaloo.teams.TeamsApplication;
import com.mubaloo.teams.R;
import com.mubaloo.teams.model.Team;
import com.mubaloo.teams.model.TeamMember;
import com.mubaloo.teams.rest.MubalooWebService;
import com.mubaloo.teams.view.SplashView;
import com.mubaloo.teams.view.TeamsActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Class implements "SplashPresenter" interface
 * Created by saber on 2016. 06. 24..
 */
public class SplashPresenterImpl implements SplashPresenter {


    private SplashView splashView;
    private JSONArray mubalooWebServiceResponse;

    public SplashPresenterImpl(SplashView splashView) {
        this.splashView = splashView;
    }

    @Override
    public void checkNetwork() {
        ConnectivityManager connectivityManager = (ConnectivityManager) splashView.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()){
            TeamsApplication.setOfflineMode(false);
        }else{
            Toast.makeText(splashView.getContext(), splashView.getContext().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
            TeamsApplication.setOfflineMode(true);
        }
    }

    @Override
    public void callMubalooWebService(SplashPresenter presenter) {
        String url = TeamsApplication.getWebServiceUrl();
        MubalooWebService mubalooWebService = new MubalooWebService(splashView.getContext(), presenter);
        mubalooWebService.execute(url);
    }

    @Override
    public void setMubalooWebServiceResponse(JSONArray response) {
        this.mubalooWebServiceResponse = response;
    }

    @Override
    public JSONArray getMubalooWebServiceResponse() {
        return mubalooWebServiceResponse;
    }

    @Override
    public void saveResponseToSQLiteDb() {

        JSONArray response = getMubalooWebServiceResponse();
        for (int i = 0; i < response.length(); i++) {
            JSONObject teamObject = response.optJSONObject(i);
            if(!teamObject.optString("teamName").isEmpty()){
                String teamName = teamObject.optString("teamName");
                Team team = new Team();
                team.setName(teamName);
                Team teamFromDb = new Select().from(Team.class).where("name = ?", team.getName()).executeSingle();
                if(teamFromDb == null) {
                    team.save();
                }else {
                    team = teamFromDb;
                }

                List<TeamMember> teamMemberList = new ArrayList<TeamMember>();
                JSONArray members = teamObject.optJSONArray("members");

                for (int j = 0; j < members.length(); j++){
                    JSONObject teamMemberObject = members.optJSONObject(j);
                    String id = teamMemberObject.optString("id");
                    String firstName = teamMemberObject.optString("firstName");
                    String lastName = teamMemberObject.optString("lastName");
                    String role = teamMemberObject.optString("role");
                    String profileImageURL = teamMemberObject.optString("profileImageURL");
                    Boolean isTeamLead = false;
                    if(teamMemberObject.has("teamLead") && teamMemberObject.optBoolean("teamLead") == true)
                        isTeamLead = true;

                    TeamMember teamMember = new TeamMember();
                    teamMember.setRemoteId(id);
                    teamMember.setFirstName(firstName);
                    teamMember.setLastName(lastName);
                    teamMember.setRole(role);
                    teamMember.setProfileImageUrl(profileImageURL);
                    teamMember.setTeam(team);
                    teamMember.setTeamLead(isTeamLead);
                    teamMemberList.add(teamMember);

                    TeamMember teamMemberFromDb = new Select().from(TeamMember.class).where("remote_id = ?", teamMember.getRemoteId()).executeSingle();
                    if(teamMemberFromDb == null) {
                        teamMember.save();
                    }else {
                        teamMemberFromDb.update(teamMember);
                    }
                }
            }
        }
    }

    @Override
    public void navigateToTeamsView(Context context) {
        Intent intent = new Intent(context, TeamsActivity.class);
        context.startActivity(intent);
    }

}
