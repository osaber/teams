package com.mubaloo.teams.presenter;

import android.content.Context;
import android.view.View;

import org.json.JSONArray;

/**
 * Presenter/ controller interface which controle the "SplashActivity" bihaviours
 * Created by saber on 2016. 06. 24..
 */
public interface SplashPresenter {
    /**
     * Method to checking network connection and setup "TeamsApplication.offlineMode" state to "true" if ther is no network connection else to "false"
     */
    void checkNetwork();

    /**
     * Method to call webservice
     * @param presenter
     */
    void callMubalooWebService(SplashPresenter presenter);

    /**
     * Method to set the called webservice response
     * @param response: JSONArray object
     */
    void setMubalooWebServiceResponse(JSONArray response);

    /**
     * Method to query the called webservice response
     * @return JSONArray object
     */
    JSONArray getMubalooWebServiceResponse();

    /**
     * Method to parse the JSONArray response and save data records to the SQLite database of the device
     */
    void saveResponseToSQLiteDb();

    /**
     * Method to navigate to team list result screen
     * @param context
     */
    void navigateToTeamsView(Context context);

}
