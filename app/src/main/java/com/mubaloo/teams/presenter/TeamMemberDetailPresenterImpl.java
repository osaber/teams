package com.mubaloo.teams.presenter;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.mubaloo.teams.TeamsApplication;
import com.mubaloo.teams.model.TeamMember;
import com.mubaloo.teams.view.TeamMemberDetailView;

/**
 * Class implements "TeamMemberDetailPresenter" interface
 * Created by saber on 2016. 06. 25..
 */
@SuppressLint("ParcelCreator")
public class TeamMemberDetailPresenterImpl implements TeamMemberDetailPresenter {

    /**
     * Selected team member
     */
    private TeamMember selectedTeamMember;

    private TeamMemberDetailView teamMemberDetailView;

    public TeamMemberDetailPresenterImpl(TeamMemberDetailView teamMemberDetailView) {
        this.teamMemberDetailView = teamMemberDetailView;
    }

    @Override
    public void prepareTeamMemberFromSQLiteDb() {
        selectedTeamMember = TeamMember.load(TeamMember.class, TeamsApplication.getSelectedTeamMemberId());
    }

    @Override
    public TeamMember getSelectedTeamMember() {
        return selectedTeamMember;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
