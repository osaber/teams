package com.mubaloo.teams.presenter;

/**
 * Presenter/ controller interface which controle the "TeamsActivity" bihaviours
 * Created by saber on 2016. 06. 24..
 */
public interface TeamsPresenter {

    /**
     * Method to loads team list and their team member lists from the device database
     */
    void prepareTeamListFromSQLiteDb();

    /**
     * Method to display team list and their team member lists
     */
    void showTeamListOnScreen();

    /**
     * Method to navigate to team member detail screen
     * @param id: long selected team member ID
     */
    void navigateToTeamMemberDetail(long id);

}
