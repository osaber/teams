package com.mubaloo.teams.presenter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.mubaloo.teams.TeamsApplication;
import com.mubaloo.teams.adapter.ExpandableListAdapter;
import com.mubaloo.teams.model.Team;
import com.mubaloo.teams.model.TeamMember;
import com.mubaloo.teams.view.TeamMemberDetailActivity;
import com.mubaloo.teams.view.TeamMemberDetailFragment;
import com.mubaloo.teams.view.TeamsView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by saber on 2016. 06. 24..
 */
public class TeamsPresenterImpl implements TeamsPresenter {

    private ExpandableListView expandableListView;
    private List<String> listDataHeader;
    private HashMap<String, List<TeamMember>> listDataChild;
    private TeamsView teamsView;


    public TeamsPresenterImpl(TeamsView teamsView, @NonNull ExpandableListView expandableListView) {
        this.teamsView = teamsView;
        this.expandableListView = expandableListView;
    }

    @Override
    public void prepareTeamListFromSQLiteDb() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<TeamMember>>();
        List<Team> teams = Team.all();

        for (int i=0; i < teams.size(); i++){
            Team team = teams.get(i);
            listDataHeader.add(team.getName());
            listDataChild.put(team.getName(), team.members());
        }

    }

    @Override
    public void showTeamListOnScreen() {

        // setting list adapter
        expandableListView.setAdapter(new ExpandableListAdapter(TeamsApplication.getContext(), listDataHeader, listDataChild));

        // Listview Group click listener
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return false;
            }
        });

        // Listview Group expanded listener
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(TeamsApplication.getContext(),
                        listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(TeamsApplication.getContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview on child click listener
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                TeamMember teamMember = (TeamMember) listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                final String teamMumberName = teamMember.getFirstName()+ " "+teamMember.getLastName()+ "("+ teamMember.isTeamLead() +")";
                // TODO Auto-generated method stub
                Toast.makeText(
                        TeamsApplication.getContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + teamMumberName, Toast.LENGTH_SHORT)
                        .show();
                TeamsApplication.setSelectedTeamMemberId(teamMember.getId());
                navigateToTeamMemberDetail(teamMember.getId());
                return false;
            }
        });
    }

    @Override
    public void navigateToTeamMemberDetail(long id) {
        Intent intent = new Intent(teamsView.getContext(), TeamMemberDetailActivity.class);
        intent.putExtra(TeamMemberDetailFragment.ARG_TEAM_MEMBER_ID, id);
        teamsView.getContext().startActivity(intent);
    }




}
