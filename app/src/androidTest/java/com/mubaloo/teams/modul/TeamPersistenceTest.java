package com.mubaloo.teams.modul;

import android.test.AndroidTestCase;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.mubaloo.teams.model.Team;

/**
 * Created by saber on 2016. 06. 28..
 */
public class TeamPersistenceTest extends AndroidTestCase {

    private Team testTeam;
    private static final String TEST_TEAM_NAME = "test_team_name";
    private Team controleTeam;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        testTeam = new Team();
        testTeam.setName(TEST_TEAM_NAME);
        testTeam.save();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        new Delete().from(Team.class).where("name = ?", TEST_TEAM_NAME).execute();
        testTeam = null;
        controleTeam = null;
    }

    public void testTeamInsertAndQuery(){
        controleTeam = queryTestTeam();
        assertNotNull(controleTeam);
        assertEquals(TEST_TEAM_NAME, controleTeam.getName());
    }

    private Team queryTestTeam() {
        return Team.load(Team.class, testTeam.getId());
    }

    private int countTeamByName(String name) {
        return new Select().from(Team.class).where("name = ?", name).count();
    }

    public void testTeamDelete() {
        testTeam.delete();
        assertEquals(0, countTeamByName(TEST_TEAM_NAME));
    }
}
