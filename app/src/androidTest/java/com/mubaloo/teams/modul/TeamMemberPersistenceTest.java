package com.mubaloo.teams.modul;

import android.test.AndroidTestCase;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.mubaloo.teams.model.Team;
import com.mubaloo.teams.model.TeamMember;

/**
 * Created by saber on 2016. 06. 28..
 */
public class TeamMemberPersistenceTest extends AndroidTestCase {

    private TeamMember testTeamMember;
    private Team testTeam;
    private static final String TEST_TEAM_NAME = "test_team_name";
    private static final String TEST_TREMOTE_ID = "test_remote_id";
    private static final String TEST_FIRST_NAME = "test_first_name";
    private static final String TEST_LAST_NAME = "test_last_name";
    private static final String TEST_ROLE = "test_role";
    private static final String TEST_PROFILE_IMAGE_URL = "test_profile_image_url";
    private static final boolean TEST_TEAM_LEAD = true;
    private TeamMember controleTeamMember;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        testTeam = new Team();
        testTeam.setName(TEST_TEAM_NAME);
        testTeam.save();

        testTeamMember = new TeamMember();
        testTeamMember.setRemoteId(TEST_TREMOTE_ID);
        testTeamMember.setFirstName(TEST_FIRST_NAME);
        testTeamMember.setLastName(TEST_LAST_NAME);
        testTeamMember.setRole(TEST_ROLE);
        testTeamMember.setProfileImageUrl(TEST_PROFILE_IMAGE_URL);
        testTeamMember.setTeamLead(TEST_TEAM_LEAD);
        testTeamMember.setTeam(testTeam);
        testTeamMember.save();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        new Delete().from(TeamMember.class).where("remote_id = ?", TEST_TREMOTE_ID).execute();
        testTeamMember = null;
        controleTeamMember = null;
    }

    public void testTeamInsertAndQuery(){
        controleTeamMember = queryTestTeamMember();
        assertNotNull(controleTeamMember);
        assertEquals(TEST_TREMOTE_ID, controleTeamMember.getRemoteId());
        assertEquals(TEST_FIRST_NAME, controleTeamMember.getFirstName());
        assertEquals(TEST_LAST_NAME, controleTeamMember.getLastName());
        assertEquals(TEST_ROLE, controleTeamMember.getRole());
        assertEquals(TEST_PROFILE_IMAGE_URL, controleTeamMember.getProfileImageUrl());
        assertEquals(TEST_TEAM_LEAD, controleTeamMember.isTeamLead());
        assertEquals(testTeam, controleTeamMember.getTeam());
    }

    private TeamMember queryTestTeamMember() {
        return TeamMember.load(TeamMember.class, testTeamMember.getId());
    }

    private int countTeamByRemoteId(String remoteId) {
        return new Select().from(TeamMember.class).where("remote_id = ?", remoteId).count();
    }

    public void testTeamDelete() {
        testTeamMember.delete();
        assertEquals(0, countTeamByRemoteId(TEST_TREMOTE_ID));
    }
}
