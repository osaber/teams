package com.mubaloo.teams;

import android.app.Application;
import android.test.ApplicationTestCase;
import com.activeandroid.util.SQLiteUtils;

/**
 * Test class
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {

    private static  final String[] tables = {
            "team_member",
            "team"
    };

    public ApplicationTest() {
        super(Application.class);
    }

    @Override
    protected void setUp() throws Exception {
        clearDatabase();
    }

    @Override
    protected void tearDown() throws Exception {
        clearDatabase();
    }

    private void clearDatabase() {
        for (String table : tables)
            SQLiteUtils.execSql("DELETE FROM "+ table +";");
    }
}